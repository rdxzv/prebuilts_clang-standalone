# prebuilts_clang-standalone

Custom LLVM Toolchains for AOSPA's kernel build system.

### How to use?

1. Clone the branch containing the LLVM you want to use from this repo to `prebuilts/clang-standalone`.

2. Enable the `KERNEL_CUSTOM_LLVM` flag from `device.mk`.

```
# Kernel
KERNEL_CUSTOM_LLVM := true
```
